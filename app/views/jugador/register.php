<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

 <?php require "../app/views/parts/header.php" ?>


<table border = "1" class="table table-striped">
<div class="starter-template">
<h1>Alta de Jugador</h1>


  <form method="post" action="/jugador/register">

<div class="form-group">
    <label>Nombre:</label>
    <input type="text" class="form-control" name="nombre">
  </div>


  <label>Puesto:  </label>
  <select name="id_puesto" class="form-control">
         <?php foreach ($puestos as $puesto): ?>
          <option value= <?php echo $puesto->nombre ?> > <?php echo $puesto->nombre ?> </option>
          <?php endforeach ?>


 </select>

 <label>Fecha de Nacimiento:  </label>
  <select name="dia">
        <?php
        for ($i=1; $i<=31; $i++) {
            if ($i == date('j'))
                echo '<option value="'.$i.'" selected>'.$i.'</option>';
            else
                echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
 </select>

 <select name="mes">
        <?php
        for ($i=1; $i<=12; $i++) {
            if ($i == date('m'))
                echo '<option value="'.$i.'" selected>'.$i.'</option>';
            else
                echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
</select>

<select name="ano">
        <?php
        for($i=date('o'); $i>=1910; $i--){
            if ($i == date('o'))
                echo '<option value="'.$i.'" selected>'.$i.'</option>';
            else
                echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
</select>

 <br>
<button type="submit" class="btn btn-default">Submit</button>

  </form>
</div>
  <?php require "../app/views/parts/footer.php" ?>


</body>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<?php require "../app/views/parts/scripts.php" ?>
</html>
