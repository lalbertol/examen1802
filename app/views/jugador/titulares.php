<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <h1>Lista de Jugadores</h1>
<table border = "1" class="table table-striped">
  <tr>

    <th>id</th>
    <th>Nombre</th>
    <th>Puesto</th>
    <th>Fecha de Nacimiento</th>
    <th>Acciones</th>
  </tr>


    <?php foreach ($_SESSION['titulares'] as $jugador): ?>
    <tr>
         <td> <?php echo $jugador ->id  ?></td>
        <td> <?php echo $jugador ->nombre  ?></td>
        <td> <?php echo $jugador ->type->nombre  ?></td>
        <td> <?php echo $jugador ->nacimiento->format("d/m/Y")  ?></td>
        <!-- <td> <?php echo $jugador ->type->name  ?></td> -->
        <td>
          <a href="/jugador/quitar/<?php echo $jugador ->id ?>" > Quitar </a>
        </td>


    </tr>


    <?php endforeach ?>


</table>


 <a class="nav-link" href="/jugador">Volver </a>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>

</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
