<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <h1>Lista de Jugadores</h1>
<table border = "1" class="table table-striped">
  <tr>

    <th>id</th>
    <th>Nombre</th>
    <th>Puesto</th>
    <th>Fecha de Nacimiento</th>
    <th>Acciones</th>
  </tr>


    <?php foreach ($jugadores as $jugador): ?>
    <tr>
         <td> <?php echo $jugador ->id  ?></td>
        <td> <?php echo $jugador ->nombre  ?></td>
        <td> <?php echo $jugador ->type->nombre  ?></td>
        <td> <?php echo $jugador ->nacimiento->format("d/m/Y")  ?></td>
        <!-- <td> <?php echo $jugador ->type->name  ?></td> -->
        <td>
          <a href="/jugador/titular/<?php echo $jugador ->id ?>" > Titulares </a>
        </td>


    </tr>


    <?php endforeach ?>


</table>

  <hr>
  Paginas:
     <?php for ($i = 1;$i <= $pages;$i++){ ?>
     <?php if ($i != $page): ?>
     <a href="/jugador/index?page=<?php echo $i ?>" class="btn">
      <?php echo $i ?>
      </a>
   <?php else: ?>
    <span class="btn">
      <?php echo $i ?>
    </span>
  <?php endif ?>
  <?php } ?>
  <hr>
 <a class="nav-link" href="/jugador/viewRegister">Añadir Nuevo Jugador</a>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>

</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
