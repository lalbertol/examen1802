<?php
namespace App\Models;

use PDO;
use Core\Model;

// require_once '../core/Model.php';
/**
*
*/
class User extends Model
{

    function __construct()
    {
        $this->dateOld = strtotime($this->nacimiento);

        $this->nacimiento = new \DateTime($this->nacimiento);
    }

    public function __get($atributoDesconocido)
    {
        if (method_exists($this, $atributoDesconocido)) {
            $this->$atributoDesconocido = $this->$atributoDesconocido();
            return $this->$atributoDesconocido;
            // echo "<hr> atributo $x <hr>";
        } else {
            return "";
        }
    }

     public static function all(){

        $db = User::db();
        $statememt = $db->query('SELECT * FROM jugadores');

        //$statememt->setFetchMode(PDO::FETCH_CLASS, 'User');
        $jugadores = $statememt->fetchAll(PDO::FETCH_CLASS,User::class);

        return $jugadores;
        }

        public function type()
    {

        $db = User::db();
        $statement = $db->prepare('SELECT * FROM puestos WHERE id = :id');
        $statement->bindValue(':id', $this->id_puesto);
        $statement->execute();
        $jugador = $statement->fetchAll(PDO::FETCH_CLASS, Puestos::class)[0];

        return $jugador;
    }

      public static function paginate($size = 10){

        if(isset($_REQUEST["page"])){
            $page = (integer) $_REQUEST["page"];
        }else{
            $page = 1;
        }

        $offset = ($page - 1) * $size;

        $db = User::db();

        $statement = $db->prepare('SELECT * FROM jugadores LIMIT :pagesize OFFSET :offset');
        $statement->bindValue(":pagesize", $size, PDO::PARAM_INT);
        $statement->bindValue(":offset", $offset, PDO::PARAM_INT);
        $statement->execute();
        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS,User::class);
        return $jugadores;
    }

     public static function rowCount()
    {
        $db = User::db();

        $statement = $db->prepare('SELECT count(id) as count FROM jugadores');
        $statement->execute();
        $rowCount = $statement->fetch(PDO::FETCH_ASSOC);
        return $rowCount["count"];
    }

     public function insert()
    {
    $db = User::db();

    $stmt = $db->prepare('INSERT INTO jugadores(nombre,nacimiento,id_puesto)
        VALUES(:nombre, :nacimiento, :id_puesto)');

    $stmt->bindValue(':nombre', $this->nombre);
    $stmt->bindValue(':nacimiento', $this->nacimiento);
    $stmt->bindValue(':id_puesto', $this->id_puesto);


    return $stmt->execute();

    }

    public function find($id){

    $db = User::db();

    $stmt = $db->prepare('SELECT * FROM jugadores WHERE id=:id');
    $stmt->execute(array(':id' => $id));
    $stmt->setFetchMode(PDO::FETCH_CLASS,User::class);
    $jugador = $stmt->fetch(PDO::FETCH_CLASS);

    return $jugador;


    }
}
