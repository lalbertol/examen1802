<?php
/**
*
*/
namespace App\Models;

use PDO;
use Core\Model;
use App\Models\User;

require_once '../core/Model.php';
require_once '../app/models/User.php';
/**
*
*/
class Puestos extends Model
{

    function __construct()
    {

    }

    public function __get($atributoDesconocido)
    {
        if (method_exists($this, $atributoDesconocido)) {
            $this->$atributoDesconocido = $this->$atributoDesconocido();
            return $this->$atributoDesconocido;
        } else {
            return "";
        }
    }
     public function all()
    {
        $db = User::db();
        $statement = $db->query('SELECT * FROM puestos');
        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS, User::class);

        return $jugadores;
    }

        public function findPuesto($nombre)
    {
        $db = Puestos::db();
        $stmt = $db->prepare('SELECT * FROM puestos WHERE nombre=:nombre');
        $stmt->execute(array(':nombre' => $nombre));
        $stmt->setFetchMode(PDO::FETCH_CLASS, Puestos::class);
        $puesto = $stmt->fetch(PDO::FETCH_CLASS);

        return $puesto;
    }



}
