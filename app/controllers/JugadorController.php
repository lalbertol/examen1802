<?php
namespace App\Controllers;

use \App\Models\User;
use \App\Models\Puestos;

class JugadorController
{

    function __construct()
    {

    }

    public function index()
    {
        //$jugadores = User::all();

    $numero = 5;

            $jugadores = User::paginate($numero);
            $rowCount = User::rowCount();

            $pages = ceil($rowCount / $numero);

            isset($_REQUEST["page"]) ? $page =(int) $_REQUEST["page"] : $page = 1;
            //$page = $_REQUEST["page"];
        //var_dump($jugadores);
    require "../app/views/jugador/index.php";
    }



    public function viewRegister(){
        //echo "hola";
        $puestos = Puestos::all();
        //var_dump($puestos);
        require "../app/views/jugador/register.php";

    }

     public function register(){

        $puesto = Puestos::findPuesto($_REQUEST['id_puesto']);


        //var_dump($puesto);


         $user = new User();

         $user->nombre = $_REQUEST['nombre'];
         $user->nacimiento = $_POST['ano'].'-'.$_POST['mes'].'/'.$_POST['dia'] . " " ."00:00:00";

         $user->id_puesto = $puesto->id;

         $user->insert();
        header('Location:/jugador');

    }

    public function titular($arguments){
        $id = (int) $arguments[0];
        //var_dump($id);
        $jugador = User::find($id);

        //var_dump($jugador);

        if (isset( $_SESSION['titulares'][$id])) {
            header('Location: /jugador');
        }else{
            $_SESSION['titulares'][$id] = $jugador;
            header('Location: /jugador');
        }

        //var_dump($_SESSION['titulares']);

    }

    public function titulares(){
        //session_destroy();
        //var_dump($_SESSION['titulares']);
        require "../app/views/jugador/titulares.php";

    }

     public function quitar($arguments){
        $id = (int) $arguments[0];
        //var_dump($id);
        unset($_SESSION['titulares'][$id]);

        header('Location: /jugador/titulares');
        //var_dump($_SESSION['titulares']);

    }



}
